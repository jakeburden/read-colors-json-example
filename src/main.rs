use ansi_term::{Colour::*, Style};
use serde::Deserialize;
use serde_json::Value;
use std::fs::File;
use std::io::{BufReader, Error};
use std::path::Path;

#[derive(Deserialize, Debug)]
struct Data {
    colors: Vec<Colors>,
}

#[derive(Deserialize, Debug)]
struct Colors {
    color: String,
    code: Value,
}

fn main() -> Result<(), Error> {
    let path = "colors.json";

    let data = get_colors(path)?;

    for color in data.colors {
        let upper = color.color.to_uppercase();
        let normal = Style::default();

        let paint = match upper.as_str() {
            "BLACK" => Black.paint(upper),
            "WHITE" => White.paint(upper),
            "RED" => Red.paint(upper),
            "BLUE" => Blue.paint(upper),
            "YELLOW" => Yellow.paint(upper),
            "GREEN" => Green.paint(upper),
            _ => normal.paint(upper),
        };

        println!("{}", paint);

        let hex = color.code["hex"].as_str();
        let hex = hex.unwrap();

        println!("{}", hex);
    }

    Ok(())
}

fn get_colors<P: AsRef<Path>>(path: P) -> Result<Data, Error> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let colors: Data = serde_json::from_reader(reader)?;

    Ok(colors)
}
